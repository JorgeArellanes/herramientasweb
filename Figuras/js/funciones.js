function obtenImgen(figura,id_contenedor) {
	$.get("xmls/figuras.xml",function(xml) {
		HTMLcontent = '<div> <svg width="480" height="300" >';
		$('figura',xml).each(function(i) {
			nombre = $(this).find("nombre").text();
			if ( nombre == figura){
				etiqueta = $(this).find("etiqueta").text();
				prop = validaPropiedad(this);
				fig = crearFiguraSvg(etiqueta,prop);
				desc = $(this).find("desc").text();
				//alert(fig);
			}
		});
		HTMLcontent += fig;
		HTMLcontent += '</svg>';
		HTMLcontent += '<p>'+desc+'</p>';
		HTMLcontent += '</div>';
		//alert(HTMLcontent);
		document.getElementById(id_contenedor).innerHTML = HTMLcontent;
	});
}

function crearFiguraSvg(etiqueta,propiedades) {
	svgHTML = '';
	svgHTML += '<'+etiqueta;
	for (var k in propiedades) {
		if (propiedades.hasOwnProperty(k)) {
			value = eval('propiedades.' + k);
			if(value != null) {
				svgHTML += ' ' + k + '="' + value + '"';
				//alert(svgHTML);
			}
		}
	}
	svgHTML += '/>';
	//alert(svgHTML);
	return svgHTML;
}

function validaPropiedad(elemento) {
	var propiedades = ["cx","cy","r" ,"rx","ry","x1","y1","x2","y2","width","height","fill","points","style"];
	var h = new Object();
	var p = "";
	for (var i = 0; i < propiedades.length; i++) {
		p = $(elemento).find(propiedades[i]).text();
		if( p != 0 ) {
			h[propiedades[i]] = p;
		}
	}
	return h;
}
