$(document).ready(function() {
	$("#tabla4").click(function(){
		$("#tabla_4").delay(3000).toggle();
	});
	$("#graf4").click(function(){
		$("#graf_4").delay(3000).toggle();
	});
	$("#tabla3").click(function(){
		$("#tabla_3").delay(2000).toggle();
		
	});
	$("#graf3").click(function(){
		$("#graf_3").delay(2000).toggle();
		
	});
	$("#tabla2").click(function(){
		$("#tabla_2").delay(2000).toggle();
		
	});
	$("#graf2").click(function(){
		$("#graf_2").delay(2000).toggle();
		
	});
	$("#tabla1").click(function(){
		$("#tabla_1").delay(2000).toggle();
		
	});
	$("#graf1").click(function(){
		$("#graf_1").delay(2000).toggle();
		
	});
});
function cambiaMenu(idTable) {
	var menus = ["table1","table2","table3","table4"];
	for(i=0; i<4; i++) {
			$('#'+menus[i]).hide();
	}
	$('.slider-section').hide();
	if(idTable.indexOf("tab")==-1) {
		$('.slider-section').show();
	} else {
		$('#'+idTable).show();
	}
}
var parciales = [];
var promedios = [];
function obtenDatos() {
    var data = [];
    $.get("../xmls/alumnos.xml",function(xml){
      	$('Alumno',xml).each(function(i) {
			nombre = $(this).find("Nombre").text();
			apellidom = $(this).find("ApellidoM").text();
			apellidop = $(this).find("ApellidoP").text();
			edad = $(this).find("Edad").text();
			c1 = $(this).find("Calif1").text();
			c2 = $(this).find("Calif2").text();
			c3 = $(this).find("Calif3").text();
			prom = (parseInt(c1)+parseInt(c2)+parseInt(c3))/3;
			promedio = 0;
			extra = 0;
			if( prom > 7) {
				promedio = prom;
				extra = "-";
			} else {
				var califEx = [5,6,7,8,9,10];
				extra = califEx[Math.floor(Math.random() * califEx.length)]
				promedio = extra;
			}
			parciales.push(extra);
			promedios.push(promedio);
			data.push({
				Id: i + 1,
				Nombre: nombre,
				ApellidoM: apellidom,
				ApellidoP: apellidop,
				Edad: edad,
				Calificacion_1: c1,
				Calificacion_2: c2,
				Calificacion_3: c3,
				Extra: extra,
				Promedio: promedio
            });
		});
	});
	alert("Tabla Lista!!! :)");
    return data;
}

function datosGrafica(){
	var alumnosG = [];						
	$.get("../xmls/alumnos.xml",function(xml){
      	$('Alumno',xml).each(function(i) {
			nombre = $(this).find("Nombre").text();
			edad = $(this).find("Edad").text();
			alumnosG.push({
				Nombre: nombre,
				Edad: edad,
				Promedio: promedios[i]
			});
		});
	});
	alert("Grafica Lista!!! :)");
	return alumnosG;
}

function datosGraficaEP(){
	var generaProedios = obtenDatos();
	var alumnosG = [];						
	$.get("../xmls/alumnos.xml",function(xml){
      	$('Alumno',xml).each(function(i) {
			nombre = $(this).find("Nombre").text();
			edad = $(this).find("Edad").text();
			alumnosG.push({
				Nombre: nombre,
				data: [[ edad, promedios[i] ]]
            });
		});
	});
	alert("Grafica Lista!!! :)");
	return alumnosG;
}
