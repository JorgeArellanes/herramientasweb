-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-11-2013 a las 07:44:41
-- Versión del servidor: 5.5.33a-MariaDB
-- Versión de PHP: 5.5.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `servicecodes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `nombre` varchar(20) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`nombre`, `fecha`) VALUES
('JORGE', '2013-11-29 05:27:09'),
('JORGE', '2013-11-29 06:26:53'),
('JORGE', '2013-11-29 06:26:53'),
('JORGE', '2013-11-29 06:26:54'),
('JORGE', '2013-11-29 06:26:55'),
('JORGE', '2013-11-29 06:27:16'),
('JORGE', '2013-11-29 06:27:25'),
('JORGE', '2013-11-29 06:30:07'),
('JORGE', '2013-11-29 06:30:26'),
('JORGE', '2013-11-29 06:31:17'),
('JORGE', '2013-11-29 06:31:55'),
('JORGE', '2013-11-29 06:31:58'),
('JORGE', '2013-11-29 06:31:59'),
('JORGE', '2013-11-29 06:32:11'),
('JORGE', '2013-11-29 06:35:04'),
('JORGE', '2013-11-29 06:35:15'),
('JORGE', '2013-11-29 06:35:29'),
('JORGE', '2013-11-29 06:37:45'),
('JORGE', '2013-11-29 06:38:07'),
('JORGE', '2013-11-29 06:39:56'),
('JORGE', '2013-11-29 06:41:58'),
('JORGE', '2013-11-29 06:49:57'),
('JORGE', '2013-11-29 06:49:58'),
('JORGE', '2013-11-29 06:54:22'),
('JORGE', '2013-11-29 07:00:23'),
('JORGE', '2013-11-29 07:00:28'),
('JORGE', '2013-11-29 07:01:42'),
('JORGE', '2013-11-29 07:01:43'),
('JORGE', '2013-11-29 07:01:56'),
('JORGE', '2013-11-05 04:19:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
  `clave` varchar(3) NOT NULL,
  `servicio` varchar(20) NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`clave`, `servicio`, `precio`) VALUES
('A11', 'hosting', 25),
('A22', 'mail', 15),
('A33', 'stream', 15),
('A44', 'dns', 5),
('A55', 'script', 15),
('A66', 'cms', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
