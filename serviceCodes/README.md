#WebService realizado en php
#
#En este ejercicio se raliza un webservice, el cual provee
#al usuario que lo utiliza una lista de servicos que incluye
#su previo contrato con la empresa x de hosting.
#El webservice recibe una cadena con el nombre del cliente que solicita
#el servicio con el fin de guardar su nombre y la fecha asi como la 
#hora exacta en que consumio el servicio.
#El web service por otra parte recibe al peticion, ahce una consulta sql
#y regresa un Array con los datos obtenidos en la consulta, los cuales
#son los diversos servicios con los que se cuentan asi como sus costos.
#En la parte del cliente se diseño una pequeña pagina web
#la cual muestra la lista de servicios obtenidos del web service, con las
#modificaciones pertinentes en los precios de cada servicio, a reserva de 
#cada cliente.
#
#Se incluye un backup de la base de datos usada en el ejemplo
#Sitauda en el directotio serviceCodes/servicecodes.sql	
#
##Jorge Carreño Arellanes
