<?php
	require_once('nusoap-0/lib/nusoap.php');
	$obtenServer = new nusoap_client('http://herramientas.net/serviceCodes/serverCodesCore.php');	
	$servicios = $obtenServer->call('getServices',array('cliente' => 'JORGE'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Glossy Box | Free CSS Template</title>
	<meta name="keywords" content="glossy box, web design, colorful background, free templates, website templates, CSS, HTML" />
	<meta name="description" content="Glossy Box | free website template with a colorful background" />
	<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery.ennui.contentslider.css" rel="stylesheet" type="text/css" media="screen,projection" />
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="templatemo_wrapper_outer">
	<div id="templatemo_wrapper">
    	<div id="templatemo_header">
			<div id="site_title">
				<h1><a href="#"><strong>Planes</strong>Hosting<span>CODESCORE</span></a></h1>
			</div>
				<ul id="social_box">
					<li><a href="#"><img src="images/facebook.png" alt="facebook" /></a></li>
					<li><a href="#"><img src="images/twitter.png" alt="twitter" /></a></li>
					<li><a href="#"><img src="images/linkedin.png" alt="linkin" /></a></li>
					<li><a href="#"><img src="images/technorati.png" alt="technorati" /></a></li>
					<li><a href="#"><img src="images/myspace.png" alt="myspace" /></a></li>                
				</ul>
            <a href="#" class="templatemo_top_header" target="_blank"><img src="images/templatemo_header_top.png" title="imagen from es.hiresimage.com" alt="imagen" /></a>
			<div class="cleaner"></div>
		</div>
        <div id="templatemo_menu">
            <ul>
                <li><a href="index.html" class="current">Planes</a></li>
            </ul>    	
        </div>
        <section id="pricePlans">
		<ul id="plans">
			<li class="plan">
				
				<ul class="planContainer">
					<li class="title"><h2>Plan 1</h2></li>
						<?php 
							$mensualidad = 0;
								foreach($servicios as $serv) {
									$mensualidad = $mensualidad + (intval($serv['precio'])+20);
								}
							 ?>
					<li class="price"><?php echo '<p><strong>$'.$mensualidad.'.00</strong>/<span>Mes</span></p>' ?></li>
					<li>
						<table class = "tablaPlan">
						<thead>
							<td><span>Servicio</span></td>
							<td><span>Precio</span></td>
						</thead>
						<tbody>
							<?php 
								foreach($servicios as $serv) {
									echo '<tr>';
									echo '<td>'.$serv['servicio'].'</td>';
									echo '<td><strong>$'.(intval($serv['precio'])+20).'.00</strong></td>';
									echo '</tr>';
								}
							 ?>
						</tbody>
						</table>
					</li>
					<li class="button"><a href="#">Comprar</a></li>
				</ul>
			</li>

			<li class="plan">
				<ul class="planContainer">
					<li class="title"><h2 class="bestPlanTitle">Plan 2</h2></li>
						<?php 
							$mensualidad = 0;
								foreach($servicios as $serv) {
									$mensualidad = $mensualidad + (intval($serv['precio'])+10);
								}
						?>
					<li class="price"><?php echo '<p><strong>$'.$mensualidad.'.00</strong>/<span>Mes</span></p>' ?></li>
					<li>
						<table class = "tablaPlan">
						<thead>
							<td><span>Servicio</span></td>
							<td><span>Precio</span></td>
						</thead>
						<tbody>
							<?php 
								foreach($servicios as $serv) {
									echo '<tr>';
									echo '<td>'.$serv['servicio'].'</td>';
									echo '<td><strong>$'.(intval($serv['precio'])+10).'.00</strong></td>';
									echo '</tr>';
								}
							 ?>
						</tbody>
						</table>
					<li class="button"><a class="bestPlanButton" href="#">Comprar</a></li>
				</ul>
			</li>
			<li class="plan">
				<ul class="planContainer">
					<li class="title"><h2>Plan 3</h2></li>
						<?php 
							$mensualidad = 0;
								foreach($servicios as $serv) {
									$mensualidad = $mensualidad + (intval($serv['precio'])+4);
								}
						?>
					<li class="price"><?php echo '<p><strong>$'.$mensualidad.'.00</strong>/<span>Mes</span></p>' ?></li>
					<li>
						<table class = "tablaPlan">
						<thead>
							<td><span>Servicio</span></td>
							<td><span>Precio</span></td>
						</thead>
						<tbody>
							<?php 
								foreach($servicios as $serv) {
									echo '<tr>';
									echo '<td>'.$serv['servicio'].'</td>';
									echo '<td><strong>$'.(intval($serv['precio'])+4).'.00</strong></td>';
									echo '</tr>';
								}
							 ?>
						</tbody>
						</table>
					</li>
					<li class="button"><a href="#">Comprar</a></li>
				</ul>
			</li>
			<li class="plan">
				<ul class="planContainer">
					<li class="title"><h2>Plan 4</h2></li>
						<?php 
							$mensualidad = 0;
								foreach($servicios as $serv) {
									$mensualidad = $mensualidad + (intval($serv['precio'])+0.5);
								}
						?>
					<li class="price"><?php echo '<p><strong>$'.$mensualidad.'</strong>/<span>Mes</span></p>' ?></li>
					<li>
						<table class = "tablaPlan">
						<thead>
							<td><span>Servicio</span></td>
							<td><span>Precio</span></td>
						</thead>
						<tbody>
							<?php 
								foreach($servicios as $serv) {
									echo '<tr>';
									echo '<td>'.$serv['servicio'].'</td>';
									echo '<td><strong>$'.(intval($serv['precio'])+0.5).'</strong></td>';
									echo '</tr>';
								}
							 ?>
						</tbody>
						</table>
					</li>
					<li class="button"><a href="#">Comprar</a></li>
				</ul>
			</li>
		</ul> <!-- End ul#plans -->
	</section>		
		<div id="templatemo_footer">
			Copyright © 2013 <a href="#">CodesCore</a> | Dise&ntilde;ado por <a href="http://www.templatemo.com" rel="nofollow" target="_parent">Jorge Carre&ntilde;o Arellanes</a>
		</div>
    </div> <!-- end of wrapper -->
</div> <!-- end of wrapper_outer -->
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js'></script>
<script type='text/javascript' src='js/logging.js'></script>
</body>
</html>
