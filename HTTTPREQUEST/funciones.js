addEvent(window,'load',inicializarEventos,false);

function inicializarEventos() {
  var ref=document.getElementById('formulario');
  addEvent(ref,'submit',enviarDatos,false);
}

function enviarDatos(e) {
  if (window.event)
    window.event.returnValue=false;
  else if (e)
      e.preventDefault();
  enviarFormulario();
}

var conexion1;
function enviarFormulario() {
  conexion1=crearXMLHttpRequest()
  conexion1.onreadystatechange = procesarEventos;
  var num=document.getElementById('numero').value;
  alert('Valor de la propiedad readyState:'+conexion1.readyState);
  conexion1.open('GET','Example.php?numero='+num, true);
  conexion1.send(null);  
}

function procesarEventos() {
  alert('Valor de la propiedad readyState:'+conexion1.readyState);
  var resultados = document.getElementById("resultados");
  if(conexion1.readyState == 4) {
    resultados.innerHTML = conexion1.responseText;
  } else if (conexion1.readyState==1 || conexion1.readyState==2 || conexion1.readyState==3) {
      resultados.innerHTML = 'Procesando...';
    }
}

function addEvent(elemento,nomevento,funcion,captura) {
  if (elemento.attachEvent) {
    elemento.attachEvent('on'+nomevento,funcion);
    return true;
  } else if (elemento.addEventListener) {
      elemento.addEventListener(nomevento,funcion,captura);
      return true;
	}
	else
	return false;
}

function crearXMLHttpRequest() {
  var xmlHttp=null;
  if (window.ActiveXObject) 
    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  else if (window.XMLHttpRequest) 
      xmlHttp = new XMLHttpRequest();
  return xmlHttp;
}
