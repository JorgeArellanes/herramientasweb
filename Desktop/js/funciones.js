var nuevaVentana;
var nuevaVentanaDib;
var parciales = [];
var promedios = [];
function obtenDatos() {
    var data = [];
    $.get("xmls/alumnos.xml",function(xml){
      	$('Alumno',xml).each(function(i) {
			nombre = $(this).find("Nombre").text();
			apellidom = $(this).find("ApellidoM").text();
			apellidop = $(this).find("ApellidoP").text();
			edad = $(this).find("Edad").text();
			c1 = $(this).find("Calif1").text();
			c2 = $(this).find("Calif2").text();
			c3 = $(this).find("Calif3").text();
			prom = (parseInt(c1)+parseInt(c2)+parseInt(c3))/3;
			promedio = 0;
			extra = 0;
			if( prom > 7) {
				promedio = prom;
				extra = "-";
			} else {
				var califEx = [5,6,7,8,9,10];
				extra = califEx[Math.floor(Math.random() * califEx.length)]
				promedio = extra;
			}
			parciales.push(extra);
			promedios.push(promedio);
			data.push({
				Id: i + 1,
				Nombre: nombre,
				ApellidoM: apellidom,
				ApellidoP: apellidop,
				Edad: edad,
				Calificacion_1: c1,
				Calificacion_2: c2,
				Calificacion_3: c3,
				Extra: extra,
				Promedio: promedio
            });
		});
	});
	alert("Terminando de cargar Componentes...!!! :)");
    return data;
}

function datosGrafica(){
	var alumnosG = [];						
	$.get("xmls/alumnos.xml",function(xml){
      	$('Alumno',xml).each(function(i) {
			nombre = $(this).find("Nombre").text();
			edad = $(this).find("Edad").text();
			alumnosG.push({
				Nombre: nombre,
				Edad: edad,
				Promedio: promedios[i]
			});
		});
	});
	alert("        Escritorio Listo.\nBienbenido a Fedora online :)"+alumnosG);
	return alumnosG;
}

function datosGraficaEP(){
	var generaProedios = obtenDatos();
	var alumnosG = [];						
	$.get("xmls/alumnos.xml",function(xml){
      	$('Alumno',xml).each(function(i) {
			nombre = $(this).find("Nombre").text();
			edad = $(this).find("Edad").text();
			alumnosG.push({
				Nombre: nombre,
				data: [[ edad, promedios[i] ]]
            });
		});
	});
	alert("Grafica Lista!!! :)");
	return alumnosG;
}

function nueva() {
	nuevaVentana=window.open("", "jNotas","directories=no, scrollbars=no, resizable=no, width=205, height=240,status=no, toolbar=no, menubar=no, location=no");
	nuevaVentana.document.write("<!DOCTYPE html> <html> <head> <meta charset='utf-8'><title>ToDo</title>"+
	"<script src='qooxdoo/application/todo/script/q-core-3.0.1.min.js'></script>"+
	"<script src='qooxdoo/application/todo/script/q-storage-3.0.1.min.js'></script>"+
	"<script src='qooxdoo/application/todo/script/q-template-3.0.1.min.js'></script>"+
	"<script src='qooxdoo/application/todo/todo.js'></script>"+
	"<script> q.ready(function() { q.startTodo(); }); </script>"+
	"<link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>"+
	"<link href='qooxdoo/application/todo/todo.css' rel='stylesheet' type='text/css'>"+
	"</head> <body> <div id='todo'> <h1>ToDo</h1>"+
    "<div class='button' id='add'>Add</div>"+
    "<div class='button' id='clear'>Clear</div>"+
    "<ul id='tasks'><li>Loading...</li></ul> </div>"+
	"<script type='template' id='task'>"+
    "<li>"+
      "<input id='{{id}}' type='checkbox' {{#done}}checked{{/done}}>"+
      "<label for='{{id}}'>{{name}}</label>"+
    "</li> </script> </body> </html>");
    nuevaVentana.moveTo(900,200);
	nuevaVentana.document.close();
}
function Dibujo() {
	nuevaVentanaDib=nuevaVentanaDib=window.open("htmls/dibujos.html", "jDibuja"," directories=no, scrollbars=no, resizable=no, width=960, height=565, status=no, toolbar=no, menubar=no, location=no");
	
    nuevaVentanaDib.moveTo(250,91);
	nuevaVentanaDib.document.close();
}
