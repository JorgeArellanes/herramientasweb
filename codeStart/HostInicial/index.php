<?php
	require_once('../ricardo/nusoap-0/lib/nusoap.php');
	$obtenServer = new nusoap_client('http://herramientas.net/codeStart/ricardo/serverCodesCore.php');	
	$servicios = $obtenServer->call('getServices',array('cliente' => 'JORGE'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Planes de hosting</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<div id="wrapper-bgbtm">
		<div id="header">
			<div id="logo">
				<h1><a href="#">Hosting Oaxaca</a></h1>
				<p>Hazte de un hosting o se un distribuidor<a href="http://www.freecsstemplates.org/" rel="nofollow">y forma tu propia empresa</a></p>
			</div>
		</div>
		<div id="menu">
			<ul>
				<li class="current_page_item"><a href="#">Homepage</a></li>
			</ul>
		</div>
		<div id="page">
			<div id="page-bgtop">
				<div id="page-bgbtm">
					<div id="content">
						<div class="post">
							<h2 class="title"><a href="#">Detalles de los Servicios</a></h2>
							<p class="meta">Ultima ves<a href="#"> Modificado</a> Noviembre, 27 de 2013 </p>
							<div class="entry">
							<ul>
							<li>
								<h2>Servicios</h2>
								<ul>
									<?php 
										foreach($servicios as $serv) {
											echo '<li><a href="#">';
											echo strtoupper($serv['servicio']).'<strong>		$'.(intval($serv['precio'])).'.00</strong>';
											echo '</a></li>';
										}
									 ?>
								</ul>
							</li>
						</ul>
							</div>
						</div>
					</div>
					<!-- end #content -->
					<div id="sidebar">
						<ul>
							<li>
								<h2>Servicios</h2>
								<ul>
									<li><a href="#">Hosting</a></li>
									<li><a href="#">DNS</a></li>
									<li><a href="#">Stream</a></li>
									<li><a href="#">Mail</a></li>
									<li><a href="#">Script</a></li>
									<li><a href="#">CMS</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<!-- end #sidebar -->
					<div style="clear: both;">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
