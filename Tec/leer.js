
// Aqui se carga la funcion cuando se carga completament el arbol DOm de nuestra pagina index.html 
$(document).ready(function(){ 

	// Creamos una funcion que se activa cuando se le da clic al elemento html con id=leerxml
	$("#leer").click(function(){
	$.get("estudiantes.xml",{},function(xml){ //Abrimos el archivo students.xml
      	
		// Crearemos un string HTML 
		HTMLcontent = '<br/><br/>';
	 	HTMLcontent += '<table width="100%" border="1" cellpadding="0" cellspacing="0" draggable="true" ondragstart="dragItem(this, event)" id="Item6">';
	  	HTMLcontent += '<th>Nombre</th><th>Calificaci&oacute;n&nbsp;1</th><th>Calificaci&oacute;n&nbsp;2</th><th>Calificaci&oacute;n&nbsp;3</th><th>Ordinario</th>';
	  	
		
		//El ciclo se va repetir cada vez que se encuentre la etiqueta estudiante
		$('estudiante',xml).each(function(i) {
			nombre = $(this).find("nombre").text();
			c1 = $(this).find("c1").text();
			c2 = $(this).find("c2").text();
			c3 = $(this).find("c3").text();
			ordinario = $(this).find("ordinario").text();
			
			//Construye una fila de la tabla y la guarda en un string
			mydata = crearFilaHtml(nombre,c1,c2,c3,ordinario);
			HTMLcontent = HTMLcontent + mydata;
		}); //final de leer cada etiqueta estudiante
		HTMLcontent += '</table>';
		
		// Actualizamos el DIV con id=ContenArea con el string html
		$("#nuevo").append(HTMLcontent);
	}); //fin de $.get
	
	});// fin de $("#leerxml").click(function(){
});//fin de document ready
 
 
 
 function crearFilaHtml(nombre,c1,c2,c3,ordianrio){
	

	// Construimo el strin HTML que contiene una fila en la tabla y lo retornamos
	filaHTML = '';
	filaHTML += '<tr>';
	filaHTML += '<td>'+ nombre + '</td>';
	filaHTML += '<td>'+ c1 +'</td>';
	filaHTML += '<td>'+ c2 +'</td>';
	filaHTML += '<td>'+ c3 +'</td>';
	filaHTML += '<td>'+ ordinario +'</td>';
	filaHTML += '</tr>';
	return filaHTML;
}


	function dragItem(item, event) {
			event.dataTransfer.setData('El Objeto', item.id)
	}
								
	function dropItem(target, event) { 
			var item = event.dataTransfer.getData('El Objeto');
	target.appendChild(document.getElementById(item));
									}
